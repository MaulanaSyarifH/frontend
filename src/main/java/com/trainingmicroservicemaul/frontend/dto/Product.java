package com.trainingmicroservicemaul.frontend.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Product {
	private String id;
    private String code;
    private String name;
    private BigDecimal price;

}
